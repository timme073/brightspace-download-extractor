import argparse
import zipfile
from pathlib import Path
import re

def unzip_file(zip_file, extract_to):
    with zipfile.ZipFile(zip_file, 'r') as zip_ref:
        zip_ref.extractall(extract_to)

def get_submission_location(submission):
    if submission.name == 'index.html':
        raise ValueError
    if 'Group' in submission.name:
        print('Submission is a group submission')
        pattern = r'Group\s\w+'

    else:
        print('Submission is a personal submission')
        pattern = '(?<= - )(.*?)(?= - )'
    matches = re.findall(pattern, submission.name)
    dir_name = matches[0].lower().replace(' ', '_')  # + '_1'
    print(dir_name)

    return submission.parent.parent / dir_name

def main(zip_location):
    # Unzip bs download
    zip_path = Path(zip_location)
    extract_location = zip_path.parent / 'bs_download'  # TODO: repace with better name?
    unzip_file(zip_location, extract_location)

    # Rename resulting directories
    for submission in extract_location.glob('*'):
        try:
            submission_location = get_submission_location(submission)
        except ValueError:
            print('skipping', submission.name)
            pass
        print(f'working on: {submission_location}')
        if submission.is_dir():
            if not submission_location.exists():
                submission.rename(submission_location)
            elif submission_location.exists():
                print('not creating directory, just moving files i guess')
            for student_file in submission_location.glob('*'):
                if student_file.suffix.lower() == '.zip':
                    unzip_file(student_file, submission_location)

    # unzip zips in resulting directories


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script that unzips a downloaded zip file from brightspace. "
                                                 "It loops through the resulting directories, renames the directoies"
                                                 "and if zip files are present unzips them.")
    parser.add_argument("dl_zip_path", help="The path of the zip of the brighspace download.")
    args = parser.parse_args()
    main(args.dl_zip_path)
