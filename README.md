# Brightspace download extractor

Small tool to unzip a brightspace download for a group project. 
It unzips the main zip file, loops through the directories renames them to `group_**` in the zip
and unzips the contained zip files.

The format for the group names might need changing for other downloads, right now
it works for geotools group projects where the group names have format `group_3a`.

The python file can be called as `python -m download-extractor.py "C:/file/path/to/zipfile.zip"`. 
The contents of the zipfile will be stored in the newly created: `C:/file/path/to/bs_download/`.



